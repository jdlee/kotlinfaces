package com.steeplesoft.kotlinfaces

import java.io.Serializable
import javax.enterprise.context.SessionScoped
import javax.inject.Named

/**
 * @author jdlee
 * @since 2015-10-29
 */
@Named
@SessionScoped
class MyBean : Serializable {
    var text = "My Text"
}
